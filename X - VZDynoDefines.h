#pragma once
// VZDynoDefines.h
// Substitutions for values that cannot be stored in the global Constants structure.
// Ex: Case statement labels.

// Tokens for data transmission and import.
//
// UPPERCASE values are Arduino --> GUI.

//	@ = time stamp
//	A = Time delta - front main sensor
//	B = Time delta - front offset sensor
//	C = Time delta - rear main sensor
//	D = Time delta - rear offset sensor
//	E = Software emergency stop button pressed
//	F = Power failure (Possible hard e-stop)
//	G = Vibration
//	H = Horsepower
//	I = Front gearbox temperature
//	J = Rear gearbox temperature
//	K = Front bearing temperature
//	L = Rear bearing temperature
//	M = Room temperature
//	N = Barometric pressure
//	O = Humidity
//	P = Front drum RPM
//	Q = Rear drum RPM
//	R = Engine RPM
//	S = Speed
//	T = Torque
//	U = Sound
//	V = Voltage
//	W = Carbon Monoxide
//	X = Terminates a list of time delta values
//	Y = Front brake tension
//	Z = Rear brake tension
//	0 = AFR sensor
//	1 = Locking pin position (LR)
//	2 = Locking pin position (LF)
//	3 = Locking pin position (RF)
//	4 = Locking pin position (RR)
//	5 = Base position
//	6 = MAINS detect
//	7 = Base proximity front
//	8 = Base proximity rear
//	9 = Front brake encoder
//	# = Rear brake encoder
//	$ = Front encoder main
//	% = Front encoder offset
//	^ = Rear encoder main
//	& = Rear encoder offset
//	* = Pendant select
//	( = Pendant execute
//	! = Pendant e-stop
//	) = Current state

// Time delta used to create Events


#define TOKEN_timestamp '@'
#define TOKEN_timeDelta_mf 'A'
#define TOKEN_timeDelta_of 'B'
#define TOKEN_timeDelta_mr 'C'
#define TOKEN_timeDelta_or 'D'
#define TOKEN_eStop 'E'
#define TOKEN_powerFailure 'F'				// Not the same as MAINS detect because it is sent when "Power has failed"
#define TOKEN_vibration 'G'
#define TOKEN_horsepower 'H'
#define TOKEN_tmp_gearbox_front 'I'
#define TOKEN_tmp_gearbox_rear 'J'
#define TOKEN_tmp_bearing_front 'K'
#define TOKEN_tmp_bearing_rear 'L'
#define TOKEN_tmp_room 'M'
#define TOKEN_pressure 'N'
#define TOKEN_humidity 'O'
#define TOKEN_rpm_drum_front 'P'
#define TOKEN_rpm_drum_rear 'Q'
#define TOKEN_rpm_engine 'R'
#define TOKEN_speed 'S'
#define TOKEN_torque 'T'
#define TOKEN_sound 'U'
#define TOKEN_voltage 'V'
#define TOKEN_CO 'W'
#define TOKEN_endDrumEncoder 'X'
#define TOKEN_tension_brake_front 'Y'
#define TOKEN_tension_brake_rear 'Z'
#define TOKEN_afr '0'
#define TOKEN_pos_lockpin_LR '1'
#define TOKEN_pos_lockpin_LF '2'
#define TOKEN_pos_lockpin_RF '3'
#define TOKEN_pos_lockpin_RR '4'
#define TOKEN_base_position '5'
#define TOKEN_mains_detect '6'
#define TOKEN_basemotor_prox_front '7'
#define TOKEN_basemotor_prox_rear '8'
#define TOKEN_brake_front_encoder '9'
#define TOKEN_brake_rear_encoder '#'
#define TOKEN_encoder_front_main '$'
#define TOKEN_encoder_front_offset '%'
#define TOKEN_encoder_rear_main '^'
#define TOKEN_encoder_rear_offset '&'
#define TOKEN_pendant_select '*'
#define TOKEN_pendant_exec '('
#define TOKEN_pendant_estop '!'
#define TOKEN_current_state ')'


// lowercase values are GUI --> Arduino.

// a = Return most recent sensor value readings
// b = Force new sensor readings and return them


#define TOKEN_changeDynoState '~'			// Tell the Dyno to change states (most likely startup)
#define TOKEN_getLastSensorValues 'a'
#define TOKEN_refreshSensorValues 'b'
#define TOKEN_start 'c'
#define TOKEN_setBase 'd'					// "d0" = Forward contactor, "d1" = reverse contactor
#define TOKEN_setBrake_front 'e'			// "e0" = On/off, "e1" = direction
#define TOKEN_setBrake_rear 'f'
#define TOKEN_setSolenoid_energize_front 'g'
#define TOKEN_setSolenoid_energize_rear 'h'
#define TOKEN_setSolenoid_hold_front 'i'
#define TOKEN_setSolenoid_hold_rear 'j'
#define TOKEN_setLP_LR 'k'
#define TOKEN_setLP_LF 'l'
#define TOKEN_setLP_RF 'm'
#define TOKEN_setLP_RR 'n'
#define TOKEN_setFans 'o'
#define TOKEN_setLED 'p'
#define TOKEN_setAWDClutch_front 'q'
#define TOKEN_setAWDClutch_rear 'r'
#define TOKEN_setGearboxCooling_front 's'
#define TOKEN_setGearboxCooling_rear 't'
#define TOKEN_setPowerConnect 'u'

// Specificity commands for motors and other devices
#define TOKEN_CMD_unknown								-1
#define TOKEN_CMD_motorOnOff							1
#define TOKEN_CMD_motorDirection						2
#define TOKEN_CMD_base_forwardContactor					3
#define TOKEN_CMD_base_reverseContactor					4
#define TOKEN_CMD_signal_LOW							10
#define TOKEN_CMD_signal_HIGH							11



#define TOKEN_Full_String _T("@ACDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopq")


// Arduino-specific
// Although these are only of importance in the control system, it doesn't hurt
// to have them in the main GUI. Moreover, it may help for logging, later.

// Current state of the system
#define DYNO_STATE_UNKNOWN					-1
#define DYNO_STATE_INIT						1			// Starting up the Arduino; control system unsure of GUI's status
#define DYNO_STATE_IDLE						2			// Waiting for commands from GUI
#define DYNO_STATE_STARTUP					3			// Prepare for vehicle
#define DYNO_STATE_ADJUST_BASE				4
#define DYNO_STATE_ADJUST_BRAKES			5
#define DYNO_STATE_ADJUST_LOCKING_PINS		6
#define DYNO_STATE_ASSESSING				7			// Dyno running, not committed to which drum is spinning and in which direction (GUI only)
#define DYNO_STATE_RUNNING					8
#define DYNO_STATE_FINISHED					9			//
#define DYNO_STATE_ESTOP_PRESSED			10
#define DYNO_STATE_POWER_FAIL				11			// Power has been cut, either through loss of power or hard e-stop press

#define DYNO_ERROR_NONE						0			// Indicates that there is no known error
#define DYNO_ERROR_PROCESSING_COMMANDS		1


// Drum state
#define DRUM_STATE_UNKNOWN					-1
#define DRUM_STATE_STOPPED					1
#define DRUM_STATE_FORWARD					2
#define DRUM_STATE_REVERSE					3

// Base position
#define BASE_UNKNOWN						(uint32_t)-1
#define BASE_HOME							(uint32_t)0
#define BASE_MAX							(uint32_t)1023		// (*) Temporary

// Result from polling serial port
#define RESULT_POLL_SERIAL_OK				0
#define RESULT_POLL_SERIAL_NODATA			1
#define RESULT_POLL_SERIAL_ERROR_RX			2
#define RESULT_POLL_SERIAL_ERROR_TX			3
#define RESULT_POLL_SERIAL_ERROR_COMM		4




// Sensors
#define SENSOR_VALUE_UNKNOWN				UINT32_MAX

// Analog sensor pins
#define PIN_SENSOR_A_WEATHER_TEMP			A0
#define PIN_SENSOR_A_WEATHER_PRESSURE		A1
#define PIN_SENSOR_A_WEATHER_HUMIDITY		A2
#define PIN_SENSOR_A_BRAKE_FRONT_TENSION	A3
#define PIN_SENSOR_A_BRAKE_REAR_TENSION		A4
#define PIN_SENSOR_A_GEARBOX_FRONT_TEMP		A5
#define PIN_SENSOR_A_GEARBOX_REAR_TEMP		A6
#define PIN_SENSOR_A_BEARING_FRONT_TEMP		A7
#define PIN_SENSOR_A_BEARING_REAR_TEMP		A8
#define PIN_SENSOR_A_LOCKPIN_LR_POS			A9
#define PIN_SENSOR_A_LOCKPIN_LF_POS			A10
#define PIN_SENSOR_A_LOCKPIN_RF_POS			A11
#define PIN_SENSOR_A_LOCKPIN_RR_POS			A12
#define PIN_SENSOR_A_RPM					A13
#define PIN_SENSOR_A_AFR					A14

// Digital sensor pins
#define PIN_SENSOR_D_ODB_II_1					0
#define PIN_SENSOR_D_ODB_II_2					1
#define PIN_BUTTON_D_PENDANT_SELECT				2
#define PIN_BUTTON_D_PENDANT_EXECUTE			3
#define PIN_ACTION_D_FANS_VFD					4
// Placeholder									5

#define PIN_ACTION_D_LOCKPIN_LR_POWER			6
#define PIN_ACTION_D_LOCKPIN_LR_DIR				7
#define PIN_ACTION_D_LOCKPIN_LF_POWER			8
#define PIN_ACTION_D_LOCKPIN_LF_DIR				9
#define PIN_ACTION_D_LOCKPIN_RF_POWER			10
#define PIN_ACTION_D_LOCKPIN_RF_DIR				11
#define PIN_ACTION_D_LOCKPIN_RR_POWER			12
#define PIN_ACTION_D_ONBOARD_LED				13
#define PIN_ACTION_D_LOCKPIN_RR_DIR				14

// Placeholder									15
// Placeholder									16
// Placeholder									17
#define PIN_SENSOR_D_ENCODER_FRONT_OFFSET		18
#define PIN_SENSOR_D_ENCODER_REAR_OFFSET		19
#define PIN_SENSOR_D_BASEMOTOR_POS				20
#define PIN_BUTTON_D_PENDANT_STOP				21

// Placeholder									22
#define PIN_ACTION_D_AWD_CLUTCH_FRONT			23
#define PIN_ACTION_D_AWD_CLUTCH_REAR			24
// Placeholder									25
#define PIN_SENSOR_D_MAINS_DETECT				26
#define PIN_ACTION_D_GEARBOX_FRONT_COOLING		27
#define PIN_ACTION_D_GEARBOX_REAR_COOLING		28
// Placeholder									29
// Placeholder									30
// Placeholder									31

#define PIN_SENSOR_D_BRAKE_FRONT_MOTOR_ENCODER	32
#define PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER	33
#define PIN_ACTION_D_BRAKE_FRONT_MOTOR_POWER	34
#define PIN_ACTION_D_BRAKE_FRONT_MOTOR_DIR		35
#define PIN_ACTION_D_BRAKE_FRONT_SOLENOID_ENG	36
#define PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD	37
#define PIN_ACTION_D_BRAKE_REAR_MOTOR_POWER		38
#define PIN_ACTION_D_BRAKE_REAR_MOTOR_DIR		39
#define PIN_ACTION_D_BRAKE_REAR_SOLENOID_ENG	40
#define PIN_ACTION_D_BRAKE_REAR_SOLENOID_HOLD	41

#define PIN_ACTION_D_MAINS_DISCONNECT			42
#define PIN_ACTION_D_BASEMOTOR_FORWARD			43
#define PIN_ACTION_D_BASEMOTOR_REVERSE			44
// Placeholder									45
// Placeholder									46
// Placeholder									47

#define PIN_SENSOR_D_ENCODER_FRONT_MAIN			48
#define PIN_SENSOR_D_ENCODER_REAR_MAIN			49

// Placeholder									50
#define PIN_SENSOR_D_BASEMOTOR_PROX_FRONT		51
#define PIN_SENSOR_D_BASEMOTOR_PROX_REAR		52
// Placeholder									53


// Hardware specific values and limitations
#define DYNO_NUM_PROCESSORS						1
#define DYNO_NUM_SENSORS						28
#define DYNO_NUM_MOTORS							15
#define DYNO_NUM_OTHER_DEVICES					11


// Custom data structures

// sensorValues stores all of the non-interrupt-based sensor values,
// even between dyno runs. The uint32_t datatype is used for ease
// of communication with the GUI, although it is twice as long as
// it needs to be.
struct sensorValues
	{
	// Analog pins
	uint32_t weather_temperature = SENSOR_VALUE_UNKNOWN;        // Room temperature
	uint32_t weather_pressure = SENSOR_VALUE_UNKNOWN;           // Barometric pressure
	uint32_t weather_humidity = SENSOR_VALUE_UNKNOWN;           // Humidity
	uint32_t brake_front_tension = SENSOR_VALUE_UNKNOWN;        // Brake load cell reading
	uint32_t brake_rear_tension = SENSOR_VALUE_UNKNOWN;
	uint32_t gearbox_front_temp = SENSOR_VALUE_UNKNOWN;         // Gearbox thermistor reading
	uint32_t gearbox_rear_temp = SENSOR_VALUE_UNKNOWN;
	uint32_t bearing_front_temp = SENSOR_VALUE_UNKNOWN;         // Bearing thermistor reading
	uint32_t bearing_rear_temp = SENSOR_VALUE_UNKNOWN;
	uint32_t lockpin_LR_pos = SENSOR_VALUE_UNKNOWN;             // Locking pin encoder
	uint32_t lockpin_LF_pos = SENSOR_VALUE_UNKNOWN;
	uint32_t lockpin_RF_pos = SENSOR_VALUE_UNKNOWN;
	uint32_t lockpin_RR_pos = SENSOR_VALUE_UNKNOWN;
	uint32_t RPM = SENSOR_VALUE_UNKNOWN;                        // RPM: <This may be moved to a digital interrupt pin>
	uint32_t AFR = SENSOR_VALUE_UNKNOWN;                        // AFR sensor

	// Digital pins
	uint32_t base_position = BASE_UNKNOWN;                      // Last known base position
	uint32_t mains_detect = SENSOR_VALUE_UNKNOWN;               // Indicates that MAINS has power 
	uint32_t basemotor_prox_front = SENSOR_VALUE_UNKNOWN;       // Indicates base is fully extended
	uint32_t basemotor_prox_rear = SENSOR_VALUE_UNKNOWN;        // Indicates base is fully retracted
	uint32_t brake_front_encoder = SENSOR_VALUE_UNKNOWN;		// Brake encoder
	uint32_t brake_rear_encoder = SENSOR_VALUE_UNKNOWN;			// 
	uint32_t encoder_front_main = SENSOR_VALUE_UNKNOWN;			// Hall effect sensors on encoder wheels
	uint32_t encoder_front_offset = SENSOR_VALUE_UNKNOWN;		//
	uint32_t encoder_rear_main = SENSOR_VALUE_UNKNOWN;			//
	uint32_t encoder_rear_offset = SENSOR_VALUE_UNKNOWN;		//
	uint32_t pendant_select = SENSOR_VALUE_UNKNOWN;				// Pendant buttons
	uint32_t pendant_exec = SENSOR_VALUE_UNKNOWN;				//
	uint32_t pendant_estop = SENSOR_VALUE_UNKNOWN;				//
	};
