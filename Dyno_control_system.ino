/* Dyno Control System (Mega)
 * 
 * Created: June 20, 2020
 * 
 * This code is adopted from the Mega code that was written to receive
 * generated data from the Nano.
 * 
 * 
 * January 2021:
 * 
 * Being officially adopted as the Dyno control system.
 * Work from this point is intended for testing and the production.
 * 
 * 
 * February 2021:
 * 
 * 
 * Basic communication functions implemented. Creating sections
 * for high-level and low-level commands
 * 
 * 
 * 
 */

#define _DYNO_DEBUG_

#include "C:/Users/cattr/Documents/Work Related/Dyno/Application/DynoMFCXP32/DynoMFCXP32/VZDynoDefines.h"

// System-wide macros and definitions
#define SERIAL_RETRY_LIMIT                      10
#define BRAKE_RELEASE_TIMEOUT                   10000       // 10 seconds to release brakes


//#define SERIAL_BUFFER_SIZE 256

#include <avr/wdt.h>                // Watchdog
#include <avr/sleep.h>


#include <HX711_ADC.h>
#if defined(ESP8266) || defined(ESP32) || defined(AVR)
#include <EEPROM.h>
#endif


/* Variable declarations and function prototypes.
 * 
 */


// Data type definitions (TO BE MOVED TO HEADER FILE)

// The sensorValues structure stores all of the non-interrupt-based sensor values,
// even between dyno runs. The uint32_t datatype is used for ease
// of communication with the GUI, although it is twice as long as
// it needs to be. It is defined in VZDynoDefines.h

bool processSensors(bool validateOnly = false);



// Third-party global definitions

//HX711 constructor:
HX711_ADC LoadCell_brake_front(PIN_SENSOR_D_BRAKE_FRONT_TENSION_DOUT, PIN_SENSOR_D_BRAKE_FRONT_TENSION_SCK);
HX711_ADC LoadCell_brake_rear(PIN_SENSOR_D_BRAKE_REAR_TENSION_DOUT, PIN_SENSOR_D_BRAKE_REAR_TENSION_SCK);
const int calVal_eepromAdress = 0;




// Hardware interrupt flags
volatile boolean flag_encoder_main_front = false;
volatile boolean flag_encoder_main_rear = false;

volatile boolean flag_encoder_offset_front = false;
volatile boolean flag_encoder_offset_rear = false;

volatile boolean flag_pendant_estop = false;
volatile boolean flag_basemotor_encoder = false;

volatile boolean flag_pendant_select = false;
volatile boolean flag_pendant_execute = false;


// Software interrupt flags
volatile boolean flag_basemotor_prox_front = false;
volatile boolean flag_basemotor_prox_rear = false;
volatile boolean flag_brake_front_encoder = false;
volatile boolean flag_brake_rear_encoder = false;

// Other timer interrupts
volatile boolean flag_poll_sensors = false;

// Polling states
boolean sendPollSensorData = false;


// Values that are persist between runs
int CURRENT_STATE = DYNO_STATE_UNKNOWN;             // Current state of the dyno
int GLOBAL_error_flag = DYNO_ERROR_NONE;
sensorValues currentSensorValues;

// NOTE: These are persistent values that may later be
// stored in NV ram.
int brakeTensionThreshold[2];


// Two-value array: STATE + last known RPM
int drum_state_front[2] = {DRUM_STATE_UNKNOWN, 0};  // Last known state of front drum
int drum_state_rear[2] = {DRUM_STATE_UNKNOWN, 0};   // Last known state of the rear drum

// Miscellaneous and internal
volatile uint16_t ovrcnt_icp4 = 0;
volatile uint16_t ovrcnt_icp5 = 0;
volatile uint32_t total_4 = 0;
volatile uint32_t total_5 = 0;
volatile uint32_t total_of = 0;
volatile uint32_t total_or = 0;

// COM variables are global for convenience
bool tokenRead = false;
int serialInputBufferSize;
char inputToken;
char inputParameter[sizeof(uint32_t)];





/* Setup - run when Arduino first powered on or last reset.
 * 
 */

// Initial setup
void setup()
{  
  cli();    // Turn off the interrupts

  // Set the state of the dyno to Initialization
  CURRENT_STATE = DYNO_STATE_INIT;

  // Set up the pins
  initializePins();

  // For communication with the PC or displaying data on the screen
  Serial.begin(115200, SERIAL_8N1);

  while (!Serial);    // Wait for serial port to connect. Needed for native USB
     
  initializeTimers();

  sei();    // Turn the interrupts back on


  set_sleep_mode(SLEEP_MODE_IDLE);  
}

/* loop - Main control loop
 * 
 */

// Main control system loop
void loop()
{
    int pollResult;
  

    // If it's in the IDLE state...
    if (CURRENT_STATE == DYNO_STATE_IDLE)
        {
        digitalWrite(PIN_ACTION_D_BRAKE_REAR_SOLENOID_ENG, LED_ON);


        }
    else
        {
        digitalWrite(PIN_ACTION_D_BRAKE_REAR_SOLENOID_ENG, LED_OFF);


        }

    // Regardless of state, communications with the GUI is always active.
    // Check for messages.

    // Poll the COM port for a token
    pollResult = pollSerial();
    digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_ENG, LED_OFF);

  
    // If there is data then set the flag
    if(pollResult == RESULT_POLL_SERIAL_OK)
    {
        // Data was read from the serial port.
      
        // Command processing is not handled directly here.
        // Rather, it is handled by the section that deals with
        // each dyno state. 

        tokenRead = true;
        digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);

    }
    else
    {
        // There is no data waiting to be processed
        tokenRead = false;
        digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_OFF);

        // If the only error is that there is no data, then it's not really a problem
        if(pollResult == RESULT_POLL_SERIAL_NODATA)
        {
        }    
        else if(pollResult == RESULT_POLL_SERIAL_ERROR_RX)
        {
            // Serial receive error      
            digitalWrite(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, LED_ON);

            // TODO: Log error
            // TODO: Take corrective action (re/establish connection, etc)    
        }
        else if(pollResult == RESULT_POLL_SERIAL_ERROR_TX)
        {
            // Serial transmit error      
            digitalWrite(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, LED_ON);

            // TODO: Log error
            // TODO: Take corrective action (re/establish connection, etc)          
        }
        else if(pollResult == RESULT_POLL_SERIAL_ERROR_COMM)
        {
            // Ambiguous serial error
            digitalWrite(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, LED_ON);

            // TODO: Log error
            // TODO: Take corrective action (re/establish connection, etc)          
        }
        else
        {
            // Illegal error condition
            digitalWrite(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, LED_ON);

            // TODO: Log error
            // TODO: Take corrective action (re/establish connection, etc)          
        }

        // Depending on the error, execution may be halted or restarted here
  
    }
  
  // Redirect control, depending on the current state

  // If in the IDLE state, MAINS power should be either confirmed,
  // and turned on if necessary. Communication with the GUI
  // should also be established.
    if(CURRENT_STATE == DYNO_STATE_INIT)
    {
        // If there is anything coming in from COM then flush
        // it. There should be no communication from the GUI
        // until after the initialization is complete.
    
        if(tokenRead)
        {
          // Flush it and ignore what we already have      
          flushIncomingData();
        }
    
        // Attempt to initialize
        if(!initializeDyno())
        {
            // TODO: Log error - could not initialize dyno

            // (*) CHECK to see if there is a timer we can use here, for the LED flash pattern (*)

            // TODO: While initializing, use a flash pattern (TBD) on the on-board LED

            // If the MAINS power is missing, use one pattern

            // If there is no connection to the PC, use a different pattern

            // If both problems exist, use another one

            return;
        }

    // When the initialization is complete, set it to Idle
    CURRENT_STATE = DYNO_STATE_IDLE;
  }
  
  // When Dyno is idle, we just want to wait for a connection from the PC
  // and nothing more. For example, if somebody starts spinning the encoder
  // wheels manually, we can log it, but we don't want to start trying
  // to transmit data to the PC.
  if(CURRENT_STATE == DYNO_STATE_IDLE)
  {
    // When the dyno is IDLE, continuously validate the sensors
      validateSensorValues();

    // In IDLE mode, commands sent from the GUI will be processed.
    // These include diagnostics-type commands, shutting down the Dyno,
    // or switching to STARTUP mode.
    if(tokenRead)
    {
        // Deal with the commands that are valid during IDLE
        if (!processCommands())
            {
            GLOBAL_error_flag = DYNO_ERROR_PROCESSING_COMMANDS;
            digitalWrite(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, LED_ON);
            return;
            }
      
    }
    
    // If the GUI had asked to continuously send sensor data (diag mode),
    // take care of that here. Only send the data both if the timer
    // has gone off, and the GUI asked for it. If we don't wait for the timer
    // then too much data is sent.
    if (flag_poll_sensors && sendPollSensorData)
        {
        if (!transmitSensorData(currentSensorValues))
            {
            // TODO: Log error sending sensor data
            GLOBAL_error_flag = DYNO_ERROR_SENDING_SENSOR_DATA;
            digitalWrite(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, LED_ON);
            return;
            }
        }

    // The IDLE state will move to the STARTUP state if three conditions are met.
    // 1: MAINS detected
    // 2: Connection to GUI verified
    // 3: GUI requests startup with parameter for base

    


    
    // Return the loop
 //   return;
  }

  // When Dyno is in the startup start, it is preparing for a vehicle.
  // Prior to this explicit switch that is requested by the GUI,
  // none of the parts on the dyno move. 
  
  if(CURRENT_STATE == DYNO_STATE_STARTUP)
  {
    // If the sensors look good then move the base, if necessary.

    // If the base position is unknown or not in the correct place,
    // then move it accordingly.

    

    
    // When the dyno is idle, continuously validate the sensors

    // Enter block if it's time to poll the sensors
    if(flag_poll_sensors)
      {
      validateSensorValues();
      flag_poll_sensors = false;
      }

    // Return the loop
    return;
  }



  // Enter block if the flag was set by the ISR
  if(flag_encoder_main_front)
  {
    cli();

    flag_encoder_main_front = false;

    // Calculate the tick count and store in smoothing array
    total_4 = (ovrcnt_icp4*65536) + ICR4;

    sei();

    // If the system is not "RUNNING" then don't report the time delta
    if(CURRENT_STATE == DYNO_STATE_RUNNING)
    {
#ifdef _DYNO_DEBUG_
      // Output to the Arduino IDE serial monitor
      Serial.print("TD-Main front: ");
      Serial.println(total_4);
#else
      // Communicate with the VZDyno GUI
      Serial.write(TOKEN_timeDelta);
      Serial.write((char*)&total_4, 4);      
#endif      
    }

    ovrcnt_icp4 = 0; 
  }

  // Enter block if the front encoder offset flag was set
  // NOTE: In the future, if it's found that there is too much
  // traffic, then the ASSESSING state will return to the control
  // system and the offsets won't be sent to the GUI once it
  // is fully running.
  if(flag_encoder_offset_front)
  {
    // If the system is not "RUNNING" then don't report the time delta
    if(CURRENT_STATE == DYNO_STATE_RUNNING)
    {
#ifdef _DYNO_DEBUG_
      Serial.println("<flag_encoder_offset_front>");

      // Output to the Arduino IDE serial monitor
      Serial.print("TD-Offset front: ");
      Serial.println(total_of);
#else
      // Communicate with the VZDyno GUI
      Serial.write(TOKEN_timeDelta);
      Serial.write((char*)&total_of, 4);      
#endif      
    }
    
    flag_encoder_offset_front = false;
  }
  

  // PLACEHOLDER for now
  // Enter block if the rear encoder main flag was set
  if(flag_encoder_main_rear)
  {
    cli();

    flag_encoder_main_rear = false;

    // Calculate the tick count and store in smoothing array
    total_5 = (ovrcnt_icp5*65536) + ICR5;

    sei();

    // If the system is not "RUNNING" then don't report the time delta
    if(CURRENT_STATE == DYNO_STATE_RUNNING)
    {
#ifdef _DYNO_DEBUG_
      // Output to the Arduino IDE serial monitor
      Serial.print("TD-Main REAR: ");
      Serial.println(total_5);
#else
      // Communicate with the VZDyno GUI
      Serial.write(TOKEN_timeDelta);
      Serial.write((char*)&total_5, 4);      
#endif      
    }

    ovrcnt_icp5 = 0; 
  }

  // Enter block if the front encoder offset flag was set
  if(flag_encoder_offset_rear)
  {
#ifdef _DYNO_DEBUG_
    Serial.println("<flag_encoder_offset_rear>");
#endif      
    
    flag_encoder_offset_rear = false;

    // If the system is not "RUNNING" then don't report the time delta
    if(CURRENT_STATE == DYNO_STATE_RUNNING)
    {
#ifdef _DYNO_DEBUG_
      Serial.println("<flag_encoder_offset_rear>");

      // Output to the Arduino IDE serial monitor
      Serial.print("TD-Offset rear: ");
      Serial.println(total_or);
#else
      // Communicate with the VZDyno GUI
      Serial.write(TOKEN_timeDelta);
      Serial.write((char*)&total_or, 4);      
#endif      
    }
    
  flag_encoder_offset_rear = false;    
  }

  // Enter block if the software e-stop button flag was set
  if(flag_pendant_estop)
    {
#ifdef _DYNO_DEBUG_
    Serial.println("<flag_pendant_estop>");
#endif      

    // Begin e-stop action
    
    flag_pendant_estop = false;
    }

  // Enter block if the base motor encoder flag was set
  if(flag_basemotor_encoder)
    {
#ifdef _DYNO_DEBUG_
    Serial.println("<flag_basemotor_encoder>");
#endif      

    // Monitor the base motor progress and respond accordingly
    
    flag_basemotor_encoder = false;
    }

  // Enter block if the pendent select button flag was set
  if(flag_pendant_select)
    {
#ifdef _DYNO_DEBUG_
    Serial.println("<flag_pendant_select>");
#endif      

    // Being pendant select action
    
    flag_pendant_select = false;
    }

  // Enter block if the pendant execute action flag was set
  if(flag_pendant_execute)
    {
#ifdef _DYNO_DEBUG_
    Serial.println("<flag_pendant_execute>");
#endif      

    // Being pendant execute action
    
    flag_pendant_execute = false;
    }

  // This is the end of the main loop
 
}

/************************************************************************************************** 
 *  High-level function definitions.
 *  
 *  Following the main loop(), all of the high-level functions
 *  control specific subsystems or individual devices.
 * 
 *************************************************************************************************/

// Initialize the dyno by checking all of the sensors
// and ensuring that MAINS is connected
bool initializeDyno()
{
  
    // Initialize the load cells first
    if (!initializeLoadCell(BRAKE_FRONT))
        {
        // TODO: Log/report the error

        return false;
        }

    if (!initializeLoadCell(BRAKE_REAR))
        {
        // TODO: Log/report the error

        return false;
        }


  // Before starting, read all of the sensors and see if the values are reasonable    
    bool validation = validateSensorValues();

    // If the sensor validation fails then abort the initialization
    if(!validation)
    {
      // TODO: Log/report the error
      
      return false;
    }

  // Check for MAINS

  // If MAINS power not connected then attempt to turn it on


  // Attempt to establish connection with the PC




  // After completing the initialization, re-read the sensors and validate them

    validation = validateSensorValues();

    // If the sensor validation fails then abort the initialization
    if(!validation)
    {
      // TODO: Log/report the error
      
      return false;
    }

  return true;
}

  // Bring the base to the home position

// Read from all of the sensors and determine whether the values are
// within an acceptable range, given the current state of the dyno.
// while in certain states, but we'll read them and check the
// The discovery of an anomalies will return false.
bool validateSensorValues()
{
  int result = true;

  // Read from the sensors but don't communicate with the GUI
  if(!processSensors(true))
  {
    // TODO: Log the error
    return false;    
  }

  // Validate each of the values to whatever extent is possible internal to the Arduino.
  // Any extreme values will be caught here. Otherwise, errors will be caught in the GUI.
  // (***) If we detect something here, then we might want to shut down the system if it's really
  // serious. However, the GUI isn't guaranteed to be connected.
  
  // TODO: Set up an on-board LED flashing function for error identification.
  // If the GUI is connected then this may be redundant, but still available.
  

  // FOR NOW:
  // Print the value of each sensor to the terminal screen with an indication of validity.
  // Once this is working, send it to the GUI.

/*  
  Serial.print("Weather temperature: ");  
  Serial.println(currentSensorValues.weather_temperature);
  
  Serial.print("Weather pressure: ");
  Serial.println(currentSensorValues.weather_pressure);
  
  Serial.print("Weather humidity: ");
  Serial.println(currentSensorValues.weather_humidity);
  
  Serial.print("Front brake pos: ");
  Serial.println(currentSensorValues.brake_front_pos);
  
  Serial.print("Rear brake pos: ");
  Serial.println(currentSensorValues.brake_rear_pos);
  
  Serial.print("Front gearbox temp: ");
  Serial.println(currentSensorValues.gearbox_front_temp);

  Serial.print("Rear gearbox temp: ");
  Serial.println(currentSensorValues.gearbox_rear_temp);

  Serial.print("Front bearing temp: ");
  Serial.println(currentSensorValues.bearing_front_temp);
  
  Serial.print("Rear bearing temp: ");
  Serial.println(currentSensorValues.bearing_rear_temp);
  
  Serial.print("LR lockpin pos: ");
  Serial.println(currentSensorValues.lockpin_LR_pos);

  Serial.print("LF lockpin pos: ");
  Serial.println(currentSensorValues.lockpin_LF_pos);

  Serial.print("RF lockpin pos: ");
  Serial.println(currentSensorValues.lockpin_RF_pos);

  Serial.print("RR lockpin pos: ");
  Serial.println(currentSensorValues.lockpin_RR_pos);
*/
    
  return result;
}




// Move the base to the specified position.
// Three pre-defined constants are BASE_UNKNOWN, BASE_HOME, and BASE_MAX.
bool repositionBase(int newPosition)
{
  int originalPosition = currentSensorValues.base_position;

  // If the base is already in the correct postion then exit
  if(newPosition == (int)currentSensorValues.base_position)
    return true;

  // Ensure the power is on
  

  // Apply the brakes


  // Lift all of the locking pins

  // (*) Note: If both pins on one side are okay but not the other side,
  // alert the operator that the dyno is out of alignment.


  // Turn on the base motor.

  // If the position is unknown then it
  // needs to be homed, first.
  if(currentSensorValues.base_position == BASE_UNKNOWN)
  {
    // Reposition to home and return an error if it fails
    if(!repositionBase(BASE_HOME))
      return false;    
  }
  
  // The relays that get turned on will be different
  // depending on what direction it needs to go in.
  if(newPosition < currentSensorValues.base_position)
  {

    
  }

  // Check position of base motor

  // If position is unknown then home it

  
  // Start moving base motor


  // Watch for e-stop button state change
  

  return true;
}


// Engage or disengage the brakes.
// BRAKE_ACTION_RELEASE: Motor will pull the brake against force of the coil
// BRAKE_ACTION_ENGAGE: Motor will pull in the reverse direction (towards failsafe)
// BRAKE_ACTION_ESTOP: Motor not used; just cut the power to the solenoid
//
// Engaging the front brake using the motor is not used for the failsafe action
// because it is too slow. It is useful for calibrating the sensors based on how
// much brake pad is left, and it can also be used to slow the drum down without
// using a sudden stop.
bool engageBrakes(int brakeAction)
{
    // Engaging or disengaging?
    // Check for the E-Stop condition first
    if(brakeAction == BRAKE_ACTION_ESTOP)
       {
        // Engaging the brakes to failsafe mode only involves cutting
        // power to the front and rear solenoids.
  
        // Turn off the hold relays.
        // Using direct PIN command to minimize delay.
        digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, BRAKE_SOLENOID_POWER_OFF);
        digitalWrite(PIN_ACTION_D_BRAKE_REAR_SOLENOID_HOLD, BRAKE_SOLENOID_POWER_OFF);

        // Turn off the energize relays (just in case)
        digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_ENG, BRAKE_SOLENOID_POWER_OFF);
        digitalWrite(PIN_ACTION_D_BRAKE_REAR_SOLENOID_ENG, BRAKE_SOLENOID_POWER_OFF);

        // Make sure that both brake motors are turned off
        digitalWrite(PIN_ACTION_D_BRAKE_FRONT_MOTOR_POWER, BRAKE_POWER_OFF);
        digitalWrite(PIN_ACTION_D_BRAKE_REAR_MOTOR_POWER, BRAKE_POWER_OFF);
        }
    else if(brakeAction == BRAKE_ACTION_ENGAGE)
       {
        // Apply the front brake slowly
        if (!applyBrake(BRAKE_FRONT))
            {
            // If the front brake fails to engage slowly then assume
            // the solenoid has to be released. Use e-stop routine.

            // Take action and ignore the result
            (void)engageBrakes(BRAKE_ACTION_ESTOP);

            // TODO: Log the issue

            // Tell the calling function that there was an error
            return false;
            }

        // Apply the rear brake slowly
        if (!applyBrake(BRAKE_REAR))
            {
            // If the rear brake fails to engage slowly then assume
            // the solenoid has to be released. Use e-stop routine.

            // Take action and ignore the result
            (void)engageBrakes(BRAKE_ACTION_ESTOP);

            // TODO: Log the issue

            // Tell the calling function that there was an error
            return false;
            }



    
       }
    else if (brakeAction == BRAKE_ACTION_RELEASE)
        {
        // Disengage the front brake using motor
        if (!releaseBrake(BRAKE_FRONT))
            {
            // Log error

            // If the front brake fails to release then just exit
            return false;
            }

        // Disengage the rear brake using motor
        if (!releaseBrake(BRAKE_REAR))
           {
            // Log error

            // If the rear brake fails then disengage the front brake and exit.
            // Safest to assume that it's an emergency when it fails.
            // Take action and ignore the result
            (void)engageBrakes(BRAKE_ACTION_ESTOP);

            // TODO: Log the issue

            // Tell the calling function that there was an error
            return false;
            }
        }
    else
        {
        // Invalid parameter

        // Log error

        return false;
      }

    return true;
}

// Release the specified brake by engaging the motor and then
// using the solenoid to hold it in place.
// Coming into here, assume that the solenoids are powered off.
// Assume that the brake is not already in the correct position.
bool releaseBrake(int brakeSpecifier)
{
    // Brake tension monitoring array
    int brakeTension_current, brakeTension_threshold;

    unsigned long timestamp_1, timestamp_2, timeElapsed = 0;

    // Dynamic pins
    int motorRunPin;
    int motorDirectionPin;
    int solenoidEngPin;
    int solenoidHoldPin;
    int sensorPin;

    // Depending on which brake is specified, use the appropriate pins and
    // the brake threshold value.
    if (brakeSpecifier == BRAKE_FRONT)
        {
        motorRunPin = PIN_ACTION_D_BRAKE_FRONT_MOTOR_POWER;
        motorDirectionPin = PIN_ACTION_D_BRAKE_FRONT_MOTOR_DIR;
        solenoidEngPin = PIN_ACTION_D_BRAKE_FRONT_SOLENOID_ENG;
        solenoidHoldPin = PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD;

//        sensorPin = PIN_SENSOR_A_BRAKE_FRONT_TENSION;

        // Tension threshold for the front brake
        brakeTension_threshold = brakeTensionThreshold[0];
        }
    else
        {
        motorRunPin = PIN_ACTION_D_BRAKE_REAR_MOTOR_POWER;
        motorDirectionPin = PIN_ACTION_D_BRAKE_REAR_MOTOR_DIR;
        solenoidEngPin = PIN_ACTION_D_BRAKE_REAR_SOLENOID_ENG;
        solenoidHoldPin = PIN_ACTION_D_BRAKE_REAR_SOLENOID_HOLD;
 
//        sensorPin = PIN_SENSOR_A_BRAKE_REAR_TENSION;

        // Tension threshold for the rear brake
        brakeTension_threshold = brakeTensionThreshold[1];
        }
    
    // Set the motor to the proper direction
    digitalWrite(motorDirectionPin, BRAKE_DIRECTION_RELEASE);

    // Keep track of when the motor was first turned on
    timestamp_1 = timestamp_2 = millis();

    // Start the motor
    digitalWrite(motorRunPin, BRAKE_POWER_ON);

    // Once the motor has started, keep monitoring the load cell.
    // Rather than read the data from all of the sensors (using processSensors(...)), it
    // is better to select only a few to avoid timing issues in a tight loop.
    do
        {
        // Check the e-stop first; rely on the interrupt setting the flag so we don't read the pin.
        // This function may seem redundant because the e-stop interrupt will cut the power, which
        // should be detected regardless. However, this will be logged differently.
        if (flag_pendant_estop)
            {
            // The last thing we knew is that we were in the middle of releasing the brake.
            // The safest thing to do is stop release the solenoids and stop the motor.

            // Take action and ignore the result
            (void)engageBrakes(BRAKE_ACTION_ESTOP);

            // TODO: Log the issue

            // Tell the calling function that there was an error
            return false;
            }

        // Make sure that we still have power
        if (!isMainsOn())
            {
            // Disengage the solenoids and stop the motors. Although there
            // is no power, the Arduino should be left in a safe state in case
            // the power comes back on.

            // Take action and ignore the result
            (void)engageBrakes(BRAKE_ACTION_ESTOP);

            // TODO: Log the issue

            // Tell the calling function that there was an error
            return false;
            }

        // Monitor the load cell tension. As the brake is
        // disengaged, the tension will increase.
        brakeTension_current = analogRead(sensorPin);

        // If there is enough tension on the brake to be confident that it is
        // released and clear of the wheel then we can continue.
        if (brakeTension_current > brakeTension_threshold)
            {
            break;
            }

        // If it makes it this far then the motor is still running.
        // After a certain amount of time, give up, since there is probably
        // something wrong.
        timestamp_1 = millis();

        // Rare, but make sure that the timer hasn't rolled over
        if (timestamp_1 < timestamp_2)
            {
            timestamp_2 = 0;
            }

        // Add the difference to the total
        timeElapsed += timestamp_2 - timestamp_1;

        if (timeElapsed > BRAKE_RELEASE_TIMEOUT)
            {
            // TODO: Log the error

            return false;
            }

        } while (true);

    // NOTE: DOUBLE-CHECK this. Energizing the solenoid here, while the motor is
    // still on, so when the motor stops the lock will be in place.
    digitalWrite(solenoidEngPin, BRAKE_SOLENOID_POWER_ON);

    // TODO: FIND OUT what has to happen here. Do we need to wait a second or
    // two after starting the hold? If so, do it now.
    // Remember - if it's a delay then continue to check messages in case the
    // e-stop button is pressed.

    // Turn off the motor
    digitalWrite(motorRunPin, BRAKE_POWER_OFF);

    // Hold the solenoid
    digitalWrite(solenoidHoldPin, BRAKE_SOLENOID_POWER_ON);

    // TODO: AGAIN - do we have to wait after applying the hold?

    // Release the energize mode
    digitalWrite(solenoidEngPin, BRAKE_SOLENOID_POWER_OFF);

    return true;
}

bool applyBrake(int brakeNumber)
{


}



bool adjustLockingPin(bool lock, int pinNumber)
{
  bool result = false; 


  return result;
}


// Stop the dyno after the soft emergency stop button has been pressed.
// This can come from the GUI or the pendant.
bool softwareEmergencyStop()
{
  bool mainsDisconnectResult = false;
  
  // Once the E-stop has been detected, disable the interrupts
  cli();
  CURRENT_STATE = DYNO_STATE_ESTOP_PRESSED;

  // Don't waste time checking the states of anything.
  // Start by turning off the power to MAINS. Then, just in
  // case it fails, turn off the relevant motors and apply the brakes.
  // The required operations and the order of them 
  // will be different, depending on the dyno's state.

  mainsDisconnectResult = switchMainsPower(false);

  // If the power disconnect is successful then this will continue as long
  // as the backup batteries are not dead.

  // TODO: Log result


  // If the base is being adjusted then the brakes will already be engaged
  if(CURRENT_STATE == DYNO_STATE_ADJUST_BASE)
  {
    // Cut power to the base motor
    
  }

  // Apply brakes
  

  // Sound alarm
  

  // Inform GUI
  


  // 


  // NOTE: We might poll the sensors manually here, before returning control
  // to the main loop or allowing the interrupt routines to do it. The post e-stop
  // information will be logged.



  sei();

  return true;
}

// Change the state of the mains power relay and verify the result.
// Parameter powerSelect:
//    true: relay will activate the MAINS contactor coil
//    false: relay will break continuity
//
// Return: State of power after operation
bool switchMainsPower(bool powerSelect)
{
  // Don't take the time to refresh the MAINS connected state, since this
  // could be an emergency stop.

  // Trigger the relay accordingly
  digitalWrite(PIN_ACTION_D_MAINS_DISCONNECT, powerSelect ? HIGH : LOW);

  // After the relay has been triggered, check and return the state of MAINS power.
  // NOTE: There may be a really tiny delay required before the reading is accurate.
  // During development, I am putting in 5ms, to be tested later.
  delay(5);

  // Return true (power on) if the read is low (As specified by this MAINS detection component)
  return(digitalRead(PIN_SENSOR_D_MAINS_DETECT) == LOW);
}

bool isMainsOn()
    {
    return digitalRead(PIN_SENSOR_D_MAINS_DETECT) == MAINS_POWER_ON;
    }


// Process commands that are valid for the current state
bool processCommands()
{
    uint16_t sendState = CURRENT_STATE;

  // Identify the command coming from the GUI

  // There are some commands that work the same, regardless of state
    switch (inputToken)
        {
        case TOKEN_getDynoState:

            // A request to get the state will immediately return with the value

            // Transmit each of the sensor data values, one at a time
            if (!sendTokenData(TOKEN_current_state, &sendState))
                {
                // TODO: Log the error

                return false;
                }

            break;

        case TOKEN_rebootArduino:

            reboot();
            return true;

        }



  // If the system is IDLE then we are interested in GUI queries
  if(CURRENT_STATE == DYNO_STATE_IDLE)
  {

    // In the IDLE state, only a few commands are valid
    switch(inputToken)
        {
        case TOKEN_changeDynoState:       // Changing states?

        // Read the token parameter (4 bytes) to see what state the request is for
            if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
                {
                // The parameter could not be read

                // TODO: Log error, and maybe do something else about it

                GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;

                return false;
                }

            // Do a bounds check, for some simple error checking
            if ((inputToken >= DYNO_STATE_INIT) && (inputToken <= DYNO_STATE_POWER_FAIL))
                {
                CURRENT_STATE = inputToken;
                return true;
                }

            // If the state change value was bad then return false and don't set the state
            return false;
        
        case TOKEN_getLastSensorValues:   // Send most recently read sensor values

        // Send the status of each sensor to the GUI
        if(!transmitSensorData(currentSensorValues))
            {
          // TODO: Log error sending sensor data
      
            GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_TX;
            digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);

            return false;
            }

        break;
  
      case TOKEN_refreshSensorValues: // Re-read the sensor values and send them

        if(!processSensors(false))
            {
            // TODO: Log error sending sensor data
      
            GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_COMM;
            digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);

            return false;
            }

        break;

      case TOKEN_start:    // 

        // Read the token parameter (4 bytes)
          if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
              {
              // The parameter could not be read

              // TODO: Log error, and maybe do something else about it

              GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
              digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);

              return false;
              }

          // Depending on the parameter value, take the appropriate action.
          // State is in 0-index: off (LOW), on (HIGH)
//          digitalWrite(PIN_ACTION_D_ONBOARD_LED, (inputParameter[0] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
          break;

      case TOKEN_setBase:    // [forward/reverse][off/on]

        // Read the token parameter (4 bytes)
          if (getTokenParameter(inputParameter, sizeof(uint32_t)))
              {
              // The parameter could not be read

              // TODO: Log error, and maybe do something else about it

              GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
              digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);

              return false;
              }

          // Depending on the parameter value, take the appropriate action.
          if (inputParameter[0] == TOKEN_CMD_base_forwardContactor)
              {
              digitalWrite(PIN_ACTION_D_BASEMOTOR_FORWARD, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }
          else if (inputParameter[0] == TOKEN_CMD_base_reverseContactor)
              {
              digitalWrite(PIN_ACTION_D_BASEMOTOR_REVERSE, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }
          else
              {
              // Invalid parameter

              // TODO: Log error

              }


          break;

      case TOKEN_setBrake_front:    // [off/on][direction]

        // Read the token parameter (4 bytes)
          if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
              {
              // The parameter could not be read

              // TODO: Log error, and maybe do something else about it

              GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
              digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
              return false;
              }

          // Depending on the parameter value, take the appropriate action.
          if (inputParameter[0] == TOKEN_CMD_motorOnOff)
              {
              // State is in 0-index: off (LOW), on (HIGH)
              digitalWrite(PIN_ACTION_D_BRAKE_FRONT_MOTOR_POWER, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }
          else if (inputParameter[0] == TOKEN_CMD_motorDirection)
              {
              // State is in 0-index: off (LOW), on (HIGH)
              digitalWrite(PIN_ACTION_D_BRAKE_FRONT_MOTOR_DIR, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }
          else
              {
              // Parameter is invalid

              // TODO: Log the error

              }

          break;

      case TOKEN_setBrake_rear:    // [off/on][direction]

        // Read the token parameter (4 bytes)
          if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
              {
              // The parameter could not be read

              // TODO: Log error, and maybe do something else about it

              GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
              digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
              return false;
              }

          // Depending on the parameter value, take the appropriate action.
          if (inputParameter[0] == TOKEN_CMD_motorOnOff)
              {
              // State is in 0-index: off (LOW), on (HIGH)
              digitalWrite(PIN_ACTION_D_BRAKE_REAR_MOTOR_POWER, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }
          else if (inputParameter[0] == TOKEN_CMD_motorDirection)
              {
              // State is in 0-index: off (LOW), on (HIGH)
              digitalWrite(PIN_ACTION_D_BRAKE_REAR_MOTOR_DIR, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }

          break;

      case TOKEN_setLP_LR:    // [off/on][direction]

        // Read the token parameter (4 bytes)
          if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
              {
              // The parameter could not be read

              // TODO: Log error, and maybe do something else about it

              GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
              digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
              return false;
              }

          // Depending on the parameter value, take the appropriate action.
          if (inputParameter[0] == TOKEN_CMD_motorOnOff)
              {
              // State is in 0-index: off (LOW), on (HIGH)
              digitalWrite(PIN_ACTION_D_LOCKPIN_LR_POWER, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }
          else if (inputParameter[0] == TOKEN_CMD_motorDirection)
              {
              // State is in 0-index: off (LOW), on (HIGH)
              digitalWrite(PIN_ACTION_D_LOCKPIN_LR_DIR, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }

          break;

      case TOKEN_setLP_LF:    // [off/on][direction]

        // Read the token parameter (4 bytes)
          if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
              {
              // The parameter could not be read

              // TODO: Log error, and maybe do something else about it

              GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
              digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
              return false;
              }

          // Depending on the parameter value, take the appropriate action.
          if (inputParameter[0] == TOKEN_CMD_motorOnOff)
              {
              // State is in 0-index: off (LOW), on (HIGH)
              digitalWrite(PIN_ACTION_D_LOCKPIN_LF_POWER, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }
          else if (inputParameter[0] == TOKEN_CMD_motorDirection)
              {
              // State is in 0-index: off (LOW), on (HIGH)
              digitalWrite(PIN_ACTION_D_LOCKPIN_LF_DIR, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }

          break;

      case TOKEN_setLP_RF:    // [off/on][direction]

        // Read the token parameter (4 bytes)
          if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
              {
              // The parameter could not be read

              // TODO: Log error, and maybe do something else about it

              GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
              digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
              return false;
              }

          // Depending on the parameter value, take the appropriate action.
          if (inputParameter[0] == TOKEN_CMD_motorOnOff)
              {
              // State is in 0-index: off (LOW), on (HIGH)
              digitalWrite(PIN_ACTION_D_LOCKPIN_RF_POWER, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }
          else if (inputParameter[0] == TOKEN_CMD_motorDirection)
              {
              // State is in 0-index: off (LOW), on (HIGH)
              digitalWrite(PIN_ACTION_D_LOCKPIN_RF_DIR, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }

          break;

      case TOKEN_setLP_RR:    // [off/on][direction]

        // Read the token parameter (4 bytes)
          if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
              {
              // The parameter could not be read

              // TODO: Log error, and maybe do something else about it

              GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
              digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
              return false;
              }

          // Depending on the parameter value, take the appropriate action.
          if (inputParameter[0] == TOKEN_CMD_motorOnOff)
              {
              // State is in 0-index: off (LOW), on (HIGH)
              digitalWrite(PIN_ACTION_D_LOCKPIN_RR_POWER, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }
          else if (inputParameter[0] == TOKEN_CMD_motorDirection)
              {
              // State is in 0-index: off (LOW), on (HIGH)
              digitalWrite(PIN_ACTION_D_LOCKPIN_RR_DIR, (inputParameter[1] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
              }

          break;

      case TOKEN_setFans:    // [off/on][?? deal with PWM setting??]

        // Read the token parameter (4 bytes)
          if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
              {
              // The parameter could not be read

              // TODO: Log error, and maybe do something else about it

              GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
              digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
              return false;
              }

          // Depending on the parameter value, take the appropriate action.

          // TODO: There is a PWM signal that has to be sent here.
          // Deal with it when we get to the fans.

          break;

        case TOKEN_setLED:    // Set the status of the on-board LED (for testing)

        // Read the token parameter (4 bytes)
        if(getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
            {
            // The parameter could not be read

            // TODO: Log error, and maybe do something else about it

            GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
            digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
            return false;
            }

        // Depending on the parameter value, take the appropriate action.
            // State is in 0-index: off (LOW), on (HIGH)

        digitalWrite(PIN_ACTION_D_ONBOARD_LED, (inputParameter[0] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
        break;

        case TOKEN_setSolenoid_energize_front:    // [on/off]

        // Read the token parameter (4 bytes)
            if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
                {
                // The parameter could not be read

                // TODO: Log error, and maybe do something else about it

                GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
                digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
                return false;
                }

            // Depending on the parameter value, take the appropriate action.
            // State is in 0-index: off (LOW), on (HIGH)
            digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_ENG, (inputParameter[0] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
            break;

        case TOKEN_setSolenoid_energize_rear:    // [on/off]

        // Read the token parameter (4 bytes)
            if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
                {
                // The parameter could not be read

                // TODO: Log error, and maybe do something else about it

                GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
                digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
                return false;
                }

            // Depending on the parameter value, take the appropriate action.
            // State is in 0-index: off (LOW), on (HIGH)
            digitalWrite(PIN_ACTION_D_BRAKE_REAR_SOLENOID_ENG, (inputParameter[0] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
            break;

        case TOKEN_setSolenoid_hold_front:    // [on/off]

        // Read the token parameter (4 bytes)
            if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
                {
                // The parameter could not be read

                // TODO: Log error, and maybe do something else about it

                GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
                digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
                return false;
                }

            // Depending on the parameter value, take the appropriate action.
            // State is in 0-index: off (LOW), on (HIGH)
            digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, (inputParameter[0] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
            break;

        case TOKEN_setSolenoid_hold_rear:    // [on/off]

        // Read the token parameter (4 bytes)
            if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
                {
                // The parameter could not be read

                // TODO: Log error, and maybe do something else about it

                GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
                digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
                return false;
                }

            // Depending on the parameter value, take the appropriate action.
            // State is in 0-index: off (LOW), on (HIGH)
            digitalWrite(PIN_ACTION_D_BRAKE_REAR_SOLENOID_HOLD, (inputParameter[0] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
            break;

        case TOKEN_setAWDClutch_front:    // [on/off]

        // Read the token parameter (4 bytes)
            if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
                {
                // The parameter could not be read

                // TODO: Log error, and maybe do something else about it

                GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
                digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
                return false;
                }

            // Depending on the parameter value, take the appropriate action.
            // State is in 0-index: off (LOW), on (HIGH)
            digitalWrite(PIN_ACTION_D_AWD_CLUTCH_FRONT, (inputParameter[0] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
            break;

        case TOKEN_setAWDClutch_rear:    // [on/off]

        // Read the token parameter (4 bytes)
            if (getTokenParameter(inputParameter, sizeof(uint32_t)))
                {
                // The parameter could not be read

                // TODO: Log error, and maybe do something else about it

                GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
                digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
                return false;
                }

            // Depending on the parameter value, take the appropriate action.
            // State is in 0-index: off (LOW), on (HIGH)
            digitalWrite(PIN_ACTION_D_AWD_CLUTCH_REAR, (inputParameter[0] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
            break;

        case TOKEN_setGearboxCooling_front:    // [on/off]

        // Read the token parameter (4 bytes)
            if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
                {
                // The parameter could not be read

                // TODO: Log error, and maybe do something else about it

                GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
                digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
                return false;
                }

            // Depending on the parameter value, take the appropriate action.
            // State is in 0-index: off (LOW), on (HIGH)
            digitalWrite(PIN_ACTION_D_GEARBOX_FRONT_COOLING, (inputParameter[0] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
            break;

        case TOKEN_setGearboxCooling_rear:    // [on/off]

        // Read the token parameter (4 bytes)
            if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
                {
                // The parameter could not be read

                // TODO: Log error, and maybe do something else about it

                GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
                digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, LED_ON);
                return false;
                }

            // Depending on the parameter value, take the appropriate action.
            // State is in 0-index: off (LOW), on (HIGH)
            digitalWrite(PIN_ACTION_D_GEARBOX_REAR_COOLING, (inputParameter[0] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
            break;

        case TOKEN_setPowerConnect:    // [on/off]

            // Read the token parameter (4 bytes)
            if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
                {
                // The parameter could not be read

                // TODO: Log error, and maybe do something else about it

                GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
                return false;
                }

            // Depending on the parameter value, take the appropriate action.
            // State is in 0-index: off (LOW), on (HIGH)
            digitalWrite(PIN_ACTION_D_MAINS_DISCONNECT, (inputParameter[0] == TOKEN_CMD_signal_LOW ? LOW : HIGH));
            break;

        case TOKEN_sensorPollingStream_begin:

            sendPollSensorData = true;
            break;

        case TOKEN_sensorPollingStream_end:

            sendPollSensorData = false;
            break;

        case TOKEN_executeProcedure:            // Execute procedure

            // Read the token parameter (4 bytes)
            if (getTokenParameter(inputParameter, sizeof(uint32_t)) != RESULT_POLL_SERIAL_OK)
                {
                // The parameter could not be read

                // TODO: Log error, and maybe do something else about it

                GLOBAL_error_flag = RESULT_POLL_SERIAL_ERROR_RX;
                return false;
                }

            // The set of procedures controls complex tasks, usually made up of smaller ones.
            // Read which procedure it is and handle it from there.
            switch (inputParameter[0])
                {
                case TOKEN_procedure_HomeBase:                      // Home the base

                    break;

                case TOKEN_procedure_MoveBaseToX:                   // Move the base to the specified position

                    break;

                case TOKEN_procedure_brakes_calibrate_front:        // Calibrate the front brake

                    // Tell the GUI that it's started
                    if (!sendResult(TOKEN_procedure_result, RESULT_executeProc_running))
                        return false;

                    // Run the calibration
                    if (!calibrateLoadCell(BRAKE_FRONT))
                        {
                        // Send back the failed result of the calibration attempt
                        (void)sendResult(TOKEN_procedure_result, RESULT_executeProc_failed);

                        // Rely on the function to set the error and return
                        return false;
                        }

                    // Tell the GUI that it's successful
                    if (!sendResult(TOKEN_procedure_result, RESULT_executeProc_passed))
                        return false;

                    break;

                case TOKEN_procedure_brakes_calibrate_rear:         // Calibrate the rear brake

                    break;

                case TOKEN_procedure_EngageBrakes:                  // Engage both brakes

                    break;

                case TOKEN_procedure_ReleaseBrakes:                 // Release both brakes

                    break;

                case TOKEN_procedure_EngageFrontBrake:              // Engage the front brake

                    break;

                case TOKEN_procedure_ReleaseFrontBrake:             // Release the front brake

                    break;

                case TOKEN_procedure_EngageRearBrake:               // Engage the rear brake

                    break;

                case TOKEN_procedure_ReleaseRearBrake:              // Release the rear brake

                    break;

                case TOKEN_procedure_EngageAWDClutches:             // Engage the AWD clutches

                    break;

                case TOKEN_procedure_ReleaseAWDClutches:            // Release the AWD clutches

                    break;

                default:        // Invalid procedure

                    break;
                }




      default:    // Invalid command
        break;
    }

/*
    // TESTING

#ifdef _DYNO_DEBUG_
    Serial.print("<Output : ");
#endif

    
    // Echo the command back to the GUI
    bytesSent = sendSerialData(inputBuffer, sizeof(uint32_t));

#ifdef _DYNO_DEBUG_
    Serial.println(">");
#endif

*/

  }


  // Assume that the result was valid
  return true;  
}





/* Low-level function definitions.
 * 
 * The low-level functionality includes specifics for
 * serial communication, timers, etc.
 * 
 */

void initializePins()
{
  // Analog pins
  pinMode(PIN_SENSOR_A_WEATHER_TEMP, INPUT_PULLUP);         // Weather station air temperature
  pinMode(PIN_SENSOR_A_WEATHER_PRESSURE, INPUT_PULLUP);     // Weather station air pressure
  pinMode(PIN_SENSOR_A_WEATHER_HUMIDITY, INPUT_PULLUP);     // Weather station humidity
  pinMode(PIN_A_3, INPUT_PULLUP);                           // Placeholder
  pinMode(PIN_A_4, INPUT_PULLUP);                           // Placeholder
  pinMode(PIN_SENSOR_A_GEARBOX_FRONT_TEMP, INPUT_PULLUP);   // Gearbox #1 (ramp side) - Gearbox grease temperature
  pinMode(PIN_SENSOR_A_GEARBOX_REAR_TEMP, INPUT_PULLUP);    // Gearbox #2 (fan side) - Gearbox grease temperature
  pinMode(PIN_SENSOR_A_BEARING_FRONT_TEMP, INPUT_PULLUP);   // Bearing #1 (ramp, outside) temperature
  pinMode(PIN_SENSOR_A_BEARING_REAR_TEMP, INPUT_PULLUP);    // Bearing #2 (fan, outside) temperature
  pinMode(PIN_SENSOR_A_LOCKPIN_LR_POS, INPUT_PULLUP);       // Locking pin #1 (left-ramp) - position
  pinMode(PIN_SENSOR_A_LOCKPIN_LF_POS, INPUT_PULLUP);       // Locking pin #2 (left-fan) - position
  pinMode(PIN_SENSOR_A_LOCKPIN_RF_POS, INPUT_PULLUP);       // Locking pin #3 (right-fan) - position
  pinMode(PIN_SENSOR_A_LOCKPIN_RR_POS, INPUT_PULLUP);       // Locking pin #4 (right-ramp) - position
  pinMode(PIN_SENSOR_A_RPM, INPUT_PULLUP);                  // RPM Induction cable
  pinMode(PIN_SENSOR_A_AFR, INPUT_PULLUP);                  // AFR sensor
    
  // Digital pins  
  pinMode(PIN_SENSOR_D_ODB_II_1, INPUT_PULLUP);             // RESERVED for ODB-II
  pinMode(PIN_SENSOR_D_ODB_II_2, INPUT_PULLUP);             // RESERVED for ODB-II

  pinMode(PIN_SENSOR_D_BASEMOTOR_PROX_FRONT, INPUT_PULLUP); // Base proximity #1
  pinMode(PIN_SENSOR_D_BASEMOTOR_PROX_REAR, INPUT_PULLUP);  // Base proximity #2
  pinMode(PIN_ACTION_D_FANS_VFD, OUTPUT);                   // VFD - Fans
  // TODO: Here - default the fans to not moving

  pinMode(5, INPUT_PULLUP);                                         //

  pinMode(PIN_ACTION_D_LOCKPIN_LR_POWER, OUTPUT);                   // Locking pin #1 (left-ramp) - on/off
  digitalWrite(PIN_ACTION_D_LOCKPIN_LR_POWER, LOCKPIN_POWER_OFF);   // Start with the power off

  pinMode(PIN_ACTION_D_LOCKPIN_LR_DIR, OUTPUT);                     // Locking pin #1 (left-ramp) - direction
  pinMode(PIN_ACTION_D_LOCKPIN_LF_POWER, OUTPUT);                   // Locking pin #2 (left-fan) - on/off
  digitalWrite(PIN_ACTION_D_LOCKPIN_LF_POWER, LOCKPIN_POWER_OFF);   // Start with the power off

  pinMode(PIN_ACTION_D_LOCKPIN_LF_DIR, OUTPUT);                     // Locking pin #2 (left-fan) - direction
  pinMode(PIN_ACTION_D_LOCKPIN_RF_POWER, OUTPUT);                   // Locking pin #3 (right-fan) - on/off
  digitalWrite(PIN_ACTION_D_LOCKPIN_RF_POWER, LOCKPIN_POWER_OFF);   // Start with the power off

  pinMode(PIN_ACTION_D_LOCKPIN_RF_DIR, OUTPUT);                     // Locking pin #3 (right-fan) - direction
  pinMode(PIN_ACTION_D_LOCKPIN_RR_POWER, OUTPUT);                   // Locking pin #4 (right-ramp) - on/off
  digitalWrite(PIN_ACTION_D_LOCKPIN_RR_POWER, LOCKPIN_POWER_OFF);   // Start with the power off

  pinMode(PIN_ACTION_D_ONBOARD_LED, OUTPUT);                        // On-board LED 
  pinMode(PIN_ACTION_D_LOCKPIN_RR_DIR, OUTPUT);                     // Locking pin #4 (right-ramp) - direction
  
  pinMode(15, INPUT_PULLUP);                                 //
  pinMode(16, INPUT_PULLUP);                                 //
  pinMode(17, INPUT_PULLUP);                                 //
  pinMode(PIN_SENSOR_D_ENCODER_FRONT_OFFSET, INPUT_PULLUP);  // Encoder wheel #2 (fan side) - offset
  pinMode(PIN_SENSOR_D_ENCODER_REAR_OFFSET, INPUT_PULLUP);   // Encoder wheel #1 (ramp side) - offset
  pinMode(PIN_SENSOR_D_BASEMOTOR_POS, INPUT_PULLUP);         // Base motor - position
  pinMode(PIN_BUTTON_D_PENDANT_STOP, INPUT_PULLUP);          // Pendant E-stop button (software emergency stop)  

  pinMode(22, INPUT_PULLUP);                                    //
  
  pinMode(PIN_ACTION_D_AWD_CLUTCH_FRONT, OUTPUT);               // Rear AWD Clutch (ramp side) - clutch - On/Off
  digitalWrite(PIN_ACTION_D_AWD_CLUTCH_FRONT, AWD_POWER_OFF);   // Start with the power off
  pinMode(PIN_ACTION_D_AWD_CLUTCH_REAR, OUTPUT);                // Front AWD Clutch (fan side) - clutch - On/Off
  digitalWrite(PIN_ACTION_D_AWD_CLUTCH_FRONT, AWD_POWER_OFF);   // Start with the power off

  pinMode(25, INPUT_PULLUP);                                    //
  pinMode(PIN_SENSOR_D_MAINS_DETECT, INPUT_PULLUP);             // Detect MAINS power detect (Voltage low=ON)
  pinMode(PIN_ACTION_D_GEARBOX_FRONT_COOLING, INPUT_PULLUP);    // RESERVED Rear Gearbox cooling control
  pinMode(PIN_ACTION_D_GEARBOX_REAR_COOLING, INPUT_PULLUP);     // RESERVED Front Gearbox cooling control
  pinMode(29, INPUT_PULLUP);                                    //
  pinMode(30, INPUT_PULLUP);                                    //
  pinMode(PIN_SENSOR_D_BRAKE_FRONT_TENSION_DOUT, INPUT_PULLUP); // Front brake load cell (tension) DOUT pin
  
  // Front brake
  pinMode(PIN_SENSOR_D_BRAKE_FRONT_MOTOR_ENCODER, OUTPUT);                    // Brake #1 - Motor - position  (*) TURN BACK TO INPUT_PULLUP AFTER TESTING COM LEDS
  digitalWrite(PIN_SENSOR_D_BRAKE_FRONT_MOTOR_ENCODER, LED_OFF);             // Start with the power off

  pinMode(PIN_ACTION_D_BRAKE_FRONT_MOTOR_POWER, OUTPUT);                            // Brake #1 - Motor - On/Off
  digitalWrite(PIN_ACTION_D_BRAKE_FRONT_MOTOR_POWER, BRAKE_POWER_OFF);              // Start with power off
  pinMode(PIN_ACTION_D_BRAKE_FRONT_MOTOR_DIR, OUTPUT);                              // Brake #1 - Motor - Direction
  pinMode(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_ENG, OUTPUT);                           // Brake #1 - Solenoid - Energize
  digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_ENG, BRAKE_SOLENOID_POWER_OFF);    // Start with power off
  pinMode(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, OUTPUT);                          // Brake #1 - Solenoid - Hold
  digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD, BRAKE_SOLENOID_POWER_OFF);   // Start with power off

  // Rear brake
  pinMode(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, OUTPUT);                     // Brake #2 - Motor - position  (*) TURN BACK TO INPUT_PULLUP AFTER TESTING COM LEDS
  digitalWrite(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, LED_OFF);             // Start with the power off

  pinMode(PIN_ACTION_D_BRAKE_REAR_MOTOR_POWER, OUTPUT);                             // Brake #2 - Motor - On/Off
  digitalWrite(PIN_ACTION_D_BRAKE_REAR_MOTOR_POWER, BRAKE_POWER_OFF);               // Start with power off
  pinMode(PIN_ACTION_D_BRAKE_REAR_MOTOR_DIR, OUTPUT);                               // Brake #2 - Motor - Direction
  pinMode(PIN_ACTION_D_BRAKE_REAR_SOLENOID_ENG, OUTPUT);                            // Brake #2 - Solenoid - Energize
  digitalWrite(PIN_ACTION_D_BRAKE_REAR_SOLENOID_ENG, BRAKE_SOLENOID_POWER_OFF);     // Start with power off
  pinMode(PIN_ACTION_D_BRAKE_REAR_SOLENOID_HOLD, OUTPUT);                           // Brake #2 - Solenoid - Hold
  digitalWrite(PIN_ACTION_D_BRAKE_REAR_SOLENOID_HOLD, BRAKE_SOLENOID_POWER_OFF);    // Start with power off

  pinMode(PIN_ACTION_D_MAINS_DISCONNECT, OUTPUT);                                   // MAINS contactor (disconnect mosfet)
  pinMode(PIN_ACTION_D_BASEMOTOR_FORWARD, OUTPUT);                                  // Base motor contactor (forward mosfet)
  digitalWrite(PIN_ACTION_D_BASEMOTOR_FORWARD, BASEMOTOR_POWER_OFF);                // Start with power off
  pinMode(PIN_ACTION_D_BASEMOTOR_REVERSE, OUTPUT);                                  // Base motor contactor (reverse mosfet)
  digitalWrite(PIN_ACTION_D_BASEMOTOR_REVERSE, BASEMOTOR_POWER_OFF);                // Start with power off

  pinMode(45, INPUT_PULLUP);                                            //
  pinMode(46, INPUT_PULLUP);                                            //
  pinMode(PIN_SENSOR_D_BRAKE_FRONT_TENSION_SCK, INPUT_PULLUP);          // Front brake load cell (tension) SCK pin

  pinMode(PIN_SENSOR_D_ENCODER_FRONT_MAIN, INPUT_PULLUP);               // Encoder wheel #2 (fan side) - main
  pinMode(PIN_SENSOR_D_ENCODER_REAR_MAIN, INPUT_PULLUP);                // Encoder wheel #1 (ramp side) - main
  pinMode(PIN_SENSOR_D_BRAKE_REAR_TENSION_DOUT, INPUT_PULLUP);          // Rear brake load cell (tension) DOUT pin

  pinMode(PIN_BUTTON_D_PENDANT_SELECT, INPUT_PULLUP);                   // Pendant button #1 (select function)
  pinMode(PIN_BUTTON_D_PENDANT_EXECUTE, INPUT_PULLUP);                  // Pendant button #2 (execute function)
  pinMode(PIN_SENSOR_D_BRAKE_REAR_TENSION_SCK, INPUT_PULLUP);           // Rear brake load cell (tension) SCK pin

  // Set the interrupts for the relevant pins
  attachInterrupt(digitalPinToInterrupt(PIN_BUTTON_D_PENDANT_STOP), fun_int_0, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_SENSOR_D_BASEMOTOR_POS), fun_int_1, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_SENSOR_D_ENCODER_REAR_OFFSET), fun_int_2, RISING);  
  attachInterrupt(digitalPinToInterrupt(PIN_SENSOR_D_ENCODER_FRONT_OFFSET), fun_int_3, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_BUTTON_D_PENDANT_SELECT), fun_int_4, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_BUTTON_D_PENDANT_EXECUTE), fun_int_5, RISING);
}

// Initialize the required timers and other interrupts
void initializeTimers()
{
  // Initialize Timer3 (poll sensor interval)
  TCCR3A = 0;
  TCCR3B = 0;

  TCNT3 = 0;
  OCR3A = 60000;
//  OCR3A = 15000;

  TCCR3A |= (1 << WGM31);               // Turn on CTC mode
  TCCR3B |= (1 << CS32) | (1 << CS30);  // Set CS22 and C20 for 1024 prescaler

  TIMSK3 |= (1 << OCIE3A);              // enable timer compare interrupt

  // No prescaler 1 (001)
  // Prescaler 1024 (101)
  // Prescaler 256  (100)
  // Prescaler 8    (010)
 
//    TCCR1B |= (1 << CS12);    // 1
//    TCCR1B &= ~(1 << CS11);   // 0
//      TCCR1B |= (1 << CS11);    // 1
//    TCCR1B |= (1 << CS10);    // 1
  
  // Initialize Timer4
 
  TCCR4A = 0;
  TCCR4B = 0;
  TCCR4C = 0;

  TIFR4 = (1<<ICF4) | (1<<TOV4);
  TCNT4 = 0;
 
  TCCR4B |= (1 << ICNC4);     // Enable denoiser
  TCCR4B &= ~(1 << ICES4);    // Capture on falling edge

  TIMSK4 |= (1 << ICIE4) | (1 << TOIE4);

  // No prescaler 1 (001)
  // Prescaler 1024 (101)
  // Prescaler 256  (100)
  // Prescaler 8    (010)
 
//    TCCR4B |= (1 << CS42);    // 1
//    TCCR4B &= ~(1 << CS41);   // 0
      TCCR4B |= (1 << CS41);    // 1
//    TCCR4B |= (1 << CS40);    // 1  
}

// Send the specified data of specified length to the serial port.
// Return the number of bytes that were successfully written.
// This function does not use the blocking write. Rather, it will
// retry sending data a preset number of times before failing.
int sendSerialData(char *buffer, int length)
{
  int retryCounter = SERIAL_RETRY_LIMIT;
  int atw;

  // Check to see if there is room to write.
  // Retry the specified number of times before giving up.
  do
    {
    atw = Serial.availableForWrite();

    // Write it all at once, or don't bother trying
    if(atw < length)
      {
//        return 0;

        digitalWrite(PIN_ACTION_D_BRAKE_REAR_SOLENOID_HOLD, LED_ON);
        digitalWrite(PIN_SENSOR_D_BRAKE_FRONT_MOTOR_ENCODER, LED_ON);

      // Exit loop on too many retries
      if(!--retryCounter)
        break;
        

      // TEST



      //delay(250);
      rpause(250);
      continue;      
      }
      
    } while (atw < length);

    digitalWrite(PIN_SENSOR_D_BRAKE_FRONT_MOTOR_ENCODER, LED_OFF);

  // If there is still 
  if(atw < length)
  {
    // TODO: log error
    digitalWrite(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, LED_ON);



    return 0;
  }

  digitalWrite(PIN_ACTION_D_BRAKE_REAR_SOLENOID_HOLD, LED_OFF);
  digitalWrite(PIN_SENSOR_D_BRAKE_FRONT_MOTOR_ENCODER, LED_OFF);

  size_t bytesWritten = Serial.write(buffer, length);

  if (bytesWritten < length)
      digitalWrite(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, LED_ON);

  // Write the data and return the length
    return bytesWritten;
}

// Communication with the GUI.
// Poll the serial port to see if there is any data available, and bring in just
// a single token. 
int pollSerial()
    {
    digitalWrite(PIN_ACTION_D_BRAKE_FRONT_SOLENOID_ENG, LED_ON);

    // Keep track of the size of the input buffer, globally
    serialInputBufferSize = Serial.available();

    // Check to see if there is any incoming data
    if(serialInputBufferSize < sizeof(char))
        return RESULT_POLL_SERIAL_NODATA;

    // Read the incoming data. Always start with a TOKEN (specified in VZDynoDefines.h)
    if(Serial.readBytes(&inputToken, sizeof(char)) < sizeof(char))
        return RESULT_POLL_SERIAL_ERROR_RX;

    // Check the length again on the way out, after reading the token
    serialInputBufferSize = Serial.available();

    // With data coming in, signal the calling function to process the commands
    return RESULT_POLL_SERIAL_OK; 
    }

// Attempt to retrieve the token parameter data that is expected
int getTokenParameter(uint32_t *parameter)
{
  return getTokenParameter((char *)parameter, sizeof(uint32_t));
}

// Attempt to retrieve the token parameter data that is expected.
// Because token parameters are expected when this function is called,
// and not optional, some allowance for latency is built in.
int getTokenParameter(char *parameter, int len)
    {
    int retryCounter = SERIAL_RETRY_LIMIT;

    do
        {
        // Keep track of the size of the input buffer, globally
        delay(100);
        serialInputBufferSize = Serial.available();

        // Verify that there is enough data, or  the retry limit has been reached
        } while ((serialInputBufferSize < len) && --retryCounter);

      // If the data wasn't there then return with nothing
      if (!retryCounter)
          {
          // This error condition indicates that a token was read but the parameter was not.

          // Just to be safe, read all of the data that is available and get rid of it
          flushIncomingData();

          digitalWrite(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, LED_ON);
          return RESULT_POLL_SERIAL_NODATA;
          }

      // Read the incoming data
      if (Serial.readBytes(parameter, len) < len)
          {
          return(RESULT_POLL_SERIAL_ERROR_RX);
          }

    // Check the buffer size again
    serialInputBufferSize = Serial.available();

    return RESULT_POLL_SERIAL_OK;
    }

// Send the specified token and data to the GUI.
// Return true if the correct number of bytes were written.
// NOTE: This is not a terribly efficient function but after
// having problems with the serial transmission, I decided
// to simplify it.
bool sendTokenData(char token, char *sendData, int length)
{
  int bytesWritten;
  bool result;

  char outputBuffer[10];
  
  outputBuffer[0] = token;
  
  // Copy the data into the parameter
  for (int i = 0; i < length; i++)
      outputBuffer[i + 1] = sendData[i];;

  // Send the aggregate field (token + data)
  bytesWritten = sendSerialData(outputBuffer, length + 1);

  if(bytesWritten < (length + 1))
      digitalWrite(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, LED_ON);

  // Return true if all of the bytes were written
  return bytesWritten == (length + 1);
}

bool sendTokenData(char token, uint16_t *sendData)
{
  return(sendTokenData(token, (char*)sendData, sizeof(uint16_t)));
}

bool sendTokenData(char token, uint32_t* sendData)
    {
    return(sendTokenData(token, (char*)sendData, sizeof(uint32_t)));
    }

// Send the result of an operation
bool sendResult(char resultToken, int resultValue)
    {
    return(sendTokenData(resultToken, (char*)&resultValue, sizeof(int)));
    }


// Read data from the sensors and send it to the serial port if required.
// This includes all of the non-interrupt-based sensors.
// There are some of these sensors that should only be valid
// while in certain states, but we'll read them regardless.
bool processSensors(bool validateOnly)
{
  // FOR NOW: This function will simply read all of the sensors and the deal with the data accordingly.
  // This will change later, when the sensors are read according to priority. For example, bearing
  // temperature will be read more often barometric pressure.

  // NOTE: Using the global sensor object is okay because we can make use of the values

  // Read the analog pins first
  currentSensorValues.weather_temperature = analogRead(PIN_SENSOR_A_WEATHER_TEMP);
  currentSensorValues.weather_pressure = analogRead(PIN_SENSOR_A_WEATHER_PRESSURE);
  currentSensorValues.weather_humidity = analogRead(PIN_SENSOR_A_WEATHER_HUMIDITY);
  currentSensorValues.gearbox_front_temp = analogRead(PIN_SENSOR_A_GEARBOX_FRONT_TEMP);
  currentSensorValues.gearbox_rear_temp = analogRead(PIN_SENSOR_A_GEARBOX_REAR_TEMP);
  currentSensorValues.bearing_front_temp = analogRead(PIN_SENSOR_A_BEARING_FRONT_TEMP);
  currentSensorValues.bearing_rear_temp = analogRead(PIN_SENSOR_A_BEARING_REAR_TEMP);
  currentSensorValues.lockpin_LR_pos = analogRead(PIN_SENSOR_A_LOCKPIN_LR_POS);
  currentSensorValues.lockpin_LF_pos = analogRead(PIN_SENSOR_A_LOCKPIN_LF_POS);
  currentSensorValues.lockpin_RF_pos = analogRead(PIN_SENSOR_A_LOCKPIN_RF_POS);
  currentSensorValues.lockpin_RR_pos = analogRead(PIN_SENSOR_A_LOCKPIN_RR_POS);
  currentSensorValues.RPM = analogRead(PIN_SENSOR_A_RPM);
  currentSensorValues.AFR = analogRead(PIN_SENSOR_A_AFR);
 
  // Read the digital pins
  currentSensorValues.base_position = digitalRead(PIN_SENSOR_D_BASEMOTOR_POS);  
  currentSensorValues.mains_detect = digitalRead(PIN_SENSOR_D_MAINS_DETECT);
  currentSensorValues.basemotor_prox_front = digitalRead(PIN_SENSOR_D_BASEMOTOR_PROX_FRONT);
  currentSensorValues.basemotor_prox_rear = digitalRead(PIN_SENSOR_D_BASEMOTOR_PROX_REAR);
  currentSensorValues.brake_front_encoder = digitalRead(PIN_SENSOR_D_BRAKE_FRONT_MOTOR_ENCODER);
  currentSensorValues.brake_rear_encoder = digitalRead(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER);
  currentSensorValues.encoder_front_main = digitalRead(PIN_SENSOR_D_ENCODER_FRONT_MAIN);
  currentSensorValues.encoder_front_offset = digitalRead(PIN_SENSOR_D_ENCODER_FRONT_OFFSET);
  currentSensorValues.encoder_rear_main = digitalRead(PIN_SENSOR_D_ENCODER_REAR_MAIN);
  currentSensorValues.encoder_rear_offset = digitalRead(PIN_SENSOR_D_ENCODER_REAR_OFFSET);
  currentSensorValues.pendant_select = digitalRead(PIN_BUTTON_D_PENDANT_SELECT);
  currentSensorValues.pendant_exec = digitalRead(PIN_BUTTON_D_PENDANT_EXECUTE);
  currentSensorValues.pendant_estop = digitalRead(PIN_BUTTON_D_PENDANT_STOP);

  //currentSensorValues.brake_front_tension = analogRead(PIN_SENSOR_A_BRAKE_FRONT_TENSION);
  //currentSensorValues.brake_rear_tension = analogRead(PIN_SENSOR_A_BRAKE_REAR_TENSION);




  // NOTE:
  // It does not make sense to poll the encoders for the base motor or brakes because
  // they need to be in motion, so they aren't checked here. 

  // If we're only going to be validating the values then return without communicating
  // them to the GUI.
  if(validateOnly)
    return true;

  // Send the status of each sensor to the GUI
  if(!transmitSensorData(currentSensorValues))
  {
    // TODO: Log error sending sensor data
      digitalWrite(PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER, LED_ON);

    return false;    
  }



#ifndef _DYNO_DEBUG_


#endif

  return true;
}

// Transmit the specified sensor data block to the GUI
bool transmitSensorData(sensorValues &output)
{
  // Transmit each of the sensor data values, one at a time
  if(!sendTokenData(TOKEN_tmp_room, &output.weather_temperature))
  {
    // TODO: Log the error
    
    return false;
  }

  if(!sendTokenData(TOKEN_pressure, &output.weather_pressure))
  {
    // TODO: Log the error
    
    return false;
  }

  if(!sendTokenData(TOKEN_humidity, &output.weather_humidity))
  {
    // TODO: Log the error
    
    return false;
  }

  if(!sendTokenData(TOKEN_tension_brake_front, &output.brake_front_tension))
  {
    // TODO: Log the error
    
    return false;
  }

  if(!sendTokenData(TOKEN_tension_brake_rear, &output.brake_rear_tension))
  {
    // TODO: Log the error
    
    return false;
  }

  if(!sendTokenData(TOKEN_tmp_gearbox_front, &output.gearbox_front_temp))
  {
    // TODO: Log the error
    
    return false;
  }

  if(!sendTokenData(TOKEN_tmp_gearbox_rear, &output.gearbox_rear_temp))
  {
    // TODO: Log the error
    
    return false;
  }
  
  if(!sendTokenData(TOKEN_tmp_bearing_front, &output.bearing_front_temp))
  {
    // TODO: Log the error
    
    return false;
  }
  
  if(!sendTokenData(TOKEN_tmp_bearing_rear, &output.bearing_rear_temp))
  {
    // TODO: Log the error
    
    return false;
  }
  
  if(!sendTokenData(TOKEN_pos_lockpin_LR, &output.lockpin_LR_pos))
  {
    // TODO: Log the error
    
    return false;
  }
  
  if(!sendTokenData(TOKEN_pos_lockpin_LF, &output.lockpin_LF_pos))
  {
    // TODO: Log the error
    
    return false;
  }
  
  if(!sendTokenData(TOKEN_pos_lockpin_RF, &output.lockpin_RF_pos))
  {
    // TODO: Log the error
    
    return false;
  }
  
  if(!sendTokenData(TOKEN_pos_lockpin_RR, &output.lockpin_RR_pos))
  {
    // TODO: Log the error
    
    return false;
  }

  if(!sendTokenData(TOKEN_rpm_engine, &output.RPM))
  {
    // TODO: Log the error
    
    return false;
  }

  if(!sendTokenData(TOKEN_afr, &output.AFR))
  {
    // TODO: Log the error
    
    return false;
  }

  if(!sendTokenData(TOKEN_base_position, &output.base_position))
  {
    // TODO: Log the error
    
    return false;
  }

  if(!sendTokenData(TOKEN_mains_detect, &output.mains_detect))
  {
    // TODO: Log the error
    
    return false;
  }
  
  if(!sendTokenData(TOKEN_basemotor_prox_front, &output.basemotor_prox_front))
  {
    // TODO: Log the error
    
    return false;
  }
  
  if(!sendTokenData(TOKEN_basemotor_prox_rear, &output.basemotor_prox_rear))
  {
    // TODO: Log the error
    
    return false;
  }  

  if(!sendTokenData(TOKEN_brake_front_encoder, &output.brake_front_encoder))
  {
    // TODO: Log the error
    
    return false;
  }
  if(!sendTokenData(TOKEN_brake_rear_encoder, &output.brake_rear_encoder))
  {
    // TODO: Log the error
    
    return false;
  }
  if(!sendTokenData(TOKEN_encoder_front_main, &output.encoder_front_main))
  {
    // TODO: Log the error
    
    return false;
  }
  if(!sendTokenData(TOKEN_encoder_front_offset, &output.encoder_front_offset))
  {
    // TODO: Log the error
    
    return false;
  }
  if(!sendTokenData(TOKEN_encoder_rear_main, &output.encoder_rear_main))
  {
    // TODO: Log the error
    
    return false;
  }
  if(!sendTokenData(TOKEN_encoder_rear_offset, &output.encoder_rear_offset))
  {
    // TODO: Log the error
    
    return false;
  }
  if(!sendTokenData(TOKEN_pendant_select, &output.pendant_select))
  {
    // TODO: Log the error
    
    return false;
  }
  if(!sendTokenData(TOKEN_pendant_exec, &output.pendant_exec))
  {
    // TODO: Log the error
    
    return false;
  }
  if(!sendTokenData(TOKEN_pendant_estop, &output.pendant_estop))
  {
    // TODO: Log the error
    
    return false;
  }

  return true;
}



// Calibrate the load cell that is used for testing tension on the brakes.
// Hardware: HX711 amplifier board (uses HX711_ADC library).
// Entering this function, there should be nothing on the load cell.
//
// NOTE: In the future, it might be possible to pass in a calibration value from the GUI
// and have it set manually. 
bool calibrateLoadCell(int brakeNumber)
    {
    HX711_ADC* loadCell;

    if (brakeNumber == BRAKE_FRONT)
        loadCell = &LoadCell_brake_front;
    else if (brakeNumber == BRAKE_REAR)
        loadCell = &LoadCell_brake_rear;
    else
        {
        GLOBAL_error_flag = DYNO_ERROR_INVALID_BRAKE_INDEX;
        return false;
        }

    unsigned long stabilizingTime = 2000;

    loadCell->begin();
    loadCell->start(2000, true);            // Start with Tare set to zero

    if (loadCell->getTareTimeoutFlag() || loadCell->getSignalTimeoutFlag())
        {
        GLOBAL_error_flag = DYNO_ERROR_LOAD_CELL_NOT_RESPONDING;
        return false;
        }

    // Set the calibration factor
    loadCell->setCalFactor(1.0);            // Manually set the calibration value (FOR NOW: Set it to 1.0)

    // Spin until the cell settles                                            
    while (!loadCell->update());
 
    // Perform the tare
    loadCell->tareNoDelay();

    // If the tare failed then exit
    if (loadCell->getTareTimeoutFlag() || loadCell->getSignalTimeoutFlag())
        {
        GLOBAL_error_flag = DYNO_ERROR_LOAD_CELL_NOT_RESPONDING;
        return false;
        }

    // Now that the scale is zeroed out, attempt to set it automatically


    // PLAN: Activate the brake motor and monitor the load cell until it stops moving,
    // then stop the brake and record the weight as some kind of maximum.
    // This assumes that the tension will continue to increase.
    // Alternative #1: Watch the motor encoder to see when it stops moving.
    // Alternative #2: Set an upper time limit and quit then.
    // The threshold will be somewhere between the min and the max, likely within about 10% of the max





    GLOBAL_error_flag = DYNO_ERROR_NONE;
    return true;
    }
/*
/// The calibration helper function is cut over from HX711_ADC
/// 
/// 
void calibrateBrakes_helper(HX711_ADC* loadCell)
    {
    Serial.println("***");
    Serial.println("Start calibration:");
    Serial.println("Place the load cell an a level stable surface.");
    Serial.println("Remove any load applied to the load cell.");
    Serial.println("Send 't' from serial monitor to set the tare offset.");

    boolean _resume = false;
    while (_resume == false) {
        LoadCell.update();
        if (Serial.available() > 0) {
            if (Serial.available() > 0) {
                char inByte = Serial.read();
                if (inByte == 't') LoadCell.tareNoDelay();
                }
            }
        if (LoadCell.getTareStatus() == true) {
            Serial.println("Tare complete");
            _resume = true;
            }
        }

    Serial.println("Now, place your known mass on the loadcell.");
    Serial.println("Then send the weight of this mass (i.e. 100.0) from serial monitor.");

    float known_mass = 0;
    _resume = false;
    while (_resume == false) {
        LoadCell.update();
        if (Serial.available() > 0) {
            known_mass = Serial.parseFloat();
            if (known_mass != 0) {
                Serial.print("Known mass is: ");
                Serial.println(known_mass);
                _resume = true;
                }
            }
        }

    LoadCell.refreshDataSet(); //refresh the dataset to be sure that the known mass is measured correct
    float newCalibrationValue = LoadCell.getNewCalibration(known_mass); //get the new calibration value

    Serial.print("New calibration value has been set to: ");
    Serial.print(newCalibrationValue);
    Serial.println(", use this as calibration value (calFactor) in your project sketch.");
    Serial.print("Save this value to EEPROM adress ");
    Serial.print(calVal_eepromAdress);
    Serial.println("? y/n");

    _resume = false;
    while (_resume == false) {
        if (Serial.available() > 0) {
            char inByte = Serial.read();
            if (inByte == 'y') {
#if defined(ESP8266)|| defined(ESP32)
                EEPROM.begin(512);
#endif
                EEPROM.put(calVal_eepromAdress, newCalibrationValue);
#if defined(ESP8266)|| defined(ESP32)
                EEPROM.commit();
#endif
                EEPROM.get(calVal_eepromAdress, newCalibrationValue);
                Serial.print("Value ");
                Serial.print(newCalibrationValue);
                Serial.print(" saved to EEPROM address: ");
                Serial.println(calVal_eepromAdress);
                _resume = true;

                }
            else if (inByte == 'n') {
                Serial.println("Value not saved to EEPROM");
                _resume = true;
                }
            }
        }

    Serial.println("End calibration");
    Serial.println("***");
    Serial.println("To re-calibrate, send 'r' from serial monitor.");
    Serial.println("For manual edit of the calibration value, send 'c' from serial monitor.");
    Serial.println("***");

    }
    */

// Initialize the specified load cells and the set the calibration parameter
// Hardware: HX711 amplifier board (uses HX711_ADC library)
bool initializeLoadCell(int brakeNumber)
    {
    HX711_ADC* loadCell;

    if (brakeNumber == BRAKE_FRONT)
        loadCell = &LoadCell_brake_front;
    else if (brakeNumber == BRAKE_REAR)
        loadCell = &LoadCell_brake_rear;
    else
        {
        GLOBAL_error_flag = DYNO_ERROR_INVALID_BRAKE_INDEX;
        return false;
        }

    unsigned long stabilizingTime = 2000;

    loadCell->begin();
    loadCell->start(2000, true);            // Start with Tare set to zero

    if (loadCell->getTareTimeoutFlag() || loadCell->getSignalTimeoutFlag())
        {
        GLOBAL_error_flag = DYNO_ERROR_LOAD_CELL_NOT_RESPONDING;
        return false;
        }

    // Set the calibration factor
    loadCell->setCalFactor(1.0);            // Manually set the calibration value (FOR NOW: Set it to 1.0)



    }


/*
// Use the HX711 library to find out what the load cell measurement is.
// 
// Return true if there was a reading, or false if there is an error.
bool getBrakeTension(int brakeNumber, )
    {
    HX711_ADC* loadCell;

    if (brakeNumber == BRAKE_FRONT)
        loadCell = &LoadCell_brake_front;
    else if (brakeNumber == BRAKE_REAR)
        loadCell = &LoadCell_brake_rear;
    else
        {
        GLOBAL_error_flag = DYNO_ERROR_INVALID_BRAKE_INDEX;
        return false;
        }

    // Read the load cell (non-blocking)
    if (loadCell->   ->wait_ready_timeout(1000))
        {
        long reading = loadcell.get_units(10);
        Serial.print("Weight: ");
        Serial.println(reading, 2);
        }
    else {
        Serial.println("HX711 not found.");
        }


    }
*/


// Flush any incoming serial data.
// The Serial.flush() command does not delete the characters in the
// the stream, so it needs to be done manually.

// Return value: true if it appears successful, but false if it appears to be a large
// or never ending stream of data. This should signal an error in the calling function.
// The number of retries are limited so that there is no endless loop here.
bool flushIncomingData()
{
    int flushLimit = 10;    // Flush the buffer up to 10 times
    int waiting;

    while(flushLimit--)
    {
      waiting = Serial.available();   // How many characters are waiting?
  
      // Loop and read all of the characters that were sitting in the buffer when it was checked
      for(int i=0; i < waiting; i++)
      {
        // NOTE: If we ever want to log the data that is being tossed then this
        // is the place to do it.
        
        (void)Serial.read();    // Read the byte and discard the result
      }
    }

  // There is no data waiting, at least as far as control is concerned
  tokenRead = false;
  serialInputBufferSize = 0;

  // Return true if there is no data in the buffer, otherwise false
  return Serial.available() == 0;
}

// Pause execution for the specific number of milliseconds
void rpause(int period)
    {
    unsigned long time_now = millis();

    while (millis() < time_now + period);
    }

// The following test is a FAIL, at least for now.
// The plan was to test each pin to see if it is connected. I had assumed that
// the static in the air would cause it to trigger more often that if a sensor,
// for example, were connected.
// As it turned out, the POT switched values 2-3 times more often that a pin
// that had nothing connected to it. I think this means that "INPUT_PULLUP"
// is more stable than a POT...
// We'll just have to rely on receiving a set of expected values.
bool testArduinoPinConnection(int pin)
{
  int changed = 0;
  int count;

  int pinValue_1 = analogRead(pin);
  int pinValue_2;
  
  for(count=0; count < 1000; count++)
  {
    pinValue_2 = analogRead(pin);

    if(pinValue_1 != pinValue_2)
    {
      pinValue_1 = pinValue_2;
      changed++;      
    }    
  }

#ifdef _DYNO_DEBUG
  Serial.print("Pin test: ");
  Serial.println(changed);
#endif

  return changed < 20;
}





// For the Arduino to reset
void reboot()
    {
    digitalWrite(PIN_ACTION_D_BRAKE_REAR_SOLENOID_HOLD, HIGH);

    wdt_disable();
    wdt_enable(WDTO_15MS);
    while (true);
    }


/* There are 4 hall effect sensors that are used to measure RPM.
 *  There are two on the front of the dyno and two at the rear.
 *  On each side, there is a main and an offset. The main will
 *  be used to calculate the RPM. The offset sensors are only
 *  used for a short time at the beginning of a run to determine
 *  the direction of the rotation.
 *  
 *  The sensors are:
 *    
 *    ICP4: Main sensor at rear
 *    ICP5: Main sensor at front
 *    INT2: Offset sensor at front
 *    INT3: Offset sensor at rear
 *    
 *
 */

// Encoder ISR (Main front)
ISR(TIMER4_CAPT_vect)  // PULSE DETECTED!  (interrupt automatically triggered, not called by main program)
{
  TCNT4 = 0; 
  flag_encoder_main_front = true;  
}

// Encoder overflow ISR
ISR (TIMER4_OVF_vect)
{
  ovrcnt_icp4++;
}

// Encoder ISR (Main rear)
ISR(TIMER5_CAPT_vect)  // PULSE DETECTED!  (interrupt automatically triggered, not called by main program)
{
  TCNT5 = 0; 
  flag_encoder_main_rear = true;  
}

// Encoder overflow ISR
ISR (TIMER5_OVF_vect)
{
  ovrcnt_icp5++;
}

// Hardware interrupt functions

// ISR for INT-0
void fun_int_0()
{
  flag_pendant_estop = true;  
}

// ISR for INT-1
void fun_int_1()
{
  flag_basemotor_encoder = true;  
}

// ISR for INT-2
void fun_int_2()
{
  flag_encoder_offset_rear = true;  
}

// ISR for INT-3
void fun_int_3()
{
  flag_encoder_offset_front = true;  
}

// ISR for INT-4
void fun_int_4()
{
  flag_pendant_select = true;  
}

// ISR for INT-5
void fun_int_5()
{
  flag_pendant_execute = true;  
}


// Sensor ISR
ISR(TIMER3_COMPA_vect)
{
  flag_poll_sensors = true;  
}


/* The following is the old loop that was used for testing.
 * 
 * 

void loop()
{
  // Enter block if the flag was set by the ISR
  if(flag_encoder)
    {
    cli();
    flag_encoder = false;

    // Calculate the tick count and store in smoothing array
    total = (ovrcnt*65536) + ICR4;

    sei();

#ifdef _DYNO_DEBUG_
      // Ouput to the Arduino IDE serial monitor
      Serial.print("TD: ");
      Serial.println(total);
#else
      // Communicate with the VZDyno GUI
      Serial.write(TOKEN_timeDelta);
      Serial.write((char*)&total, 4);      
#endif

    ovrcnt = 0; 
    }

  // Enter block if the INT.2 flag was set
  if(flag_int_2)
    {
    Serial.println("<INT.2>");    
    flag_int_2 = false;
    }

  // Enter block if the sensor flag was set by the timer ISR
  if(flag_sensor)
    {
    processSensors();
    flag_sensor = false;
    }

}


 * 
 */
